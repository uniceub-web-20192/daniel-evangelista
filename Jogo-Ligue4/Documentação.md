# Ligue 4

* Link do jogo criado para a matéria de desenvolvimento web: [Ligue4](http://ligue4.rf.gd/)

* Abaixo uma imagem do jogo:

![Ligue4](img/jogo-doc.png)

O **Ligue 4** (também conhecido como Mestra do Capitão , Connect Four, Four Up, Lote Quatro, Encontre Quatro, Quatro em Linha, Drop Four e Gravitrips (na União Soviética)) é um jogo de conexão para dois jogadores no qual os jogadores primeiro escolha uma cor e, em seguida, solte um disco colorido da parte superior em uma grade de sete colunas e seis linhas verticalmente suspensa. As peças caem para baixo, ocupando o menor espaço disponível dentro da coluna. O objetivo do jogo é ser o primeiro a formar uma linha horizontal, vertical ou diagonal de quatro discos próprios. Connect Four é uma soluçãojogos. O primeiro jogador sempre pode vencer jogando os movimentos certos.

O jogo foi vendido pela marca Connect Four por Milton Bradley em fevereiro de 1974.

## Solução matemática

**Ligue 4** é um jogo para dois jogadores com "informações perfeitas". Este termo descreve os jogos em que um jogador de cada vez joga, os jogadores têm todas as informações sobre os movimentos que ocorreram e todos os movimentos que podem ocorrer, para um determinado estado do jogo. O Ligue 4 também pertence à classificação de um jogo adversário de soma zero , já que a vantagem de um jogador é uma desvantagem do oponente.

Uma medida da complexidade do jogo Ligue 4 é o número de posições possíveis no tabuleiro de jogos. Para o clássico Ligue 4 jogado em 6 de altura e 7 de largura, existem 4.531.985.219.092 posições para todos os tabuleiros de jogo com 0 a 42 peças.

O jogo foi resolvido pela primeira vez por James Dow Allen (1 de outubro de 1988) e de forma independente por Victor Allis (16 de outubro de 1988). Allis descreve uma abordagem baseada no conhecimento, com nove estratégias, como uma solução para o Ligue 4. Allen também descreve estratégias vencedoras em sua análise do jogo. No momento das soluções iniciais para o Ligue 4, a análise de força bruta não era considerada viável, dada a complexidade do jogo e a tecnologia de computador disponível no momento.

O Ligue 4 foi resolvido com métodos de força bruta, começando com o trabalho de John Tromp na compilação de um banco de dados de 8 camadas (4 de fevereiro de 1995). Os algoritmos de inteligência artificial capazes de resolver fortemente o Ligue 4 são minimax ou negamax , com otimizações que incluem tabelas de poda alfa-beta , ordenação de movimentos e transposição . O código para resolver o Connect Four com esses métodos também é a base do benchmark de desempenho inteiro do Fhourstones.

A conclusão resolvida para o Ligue 4 é a vitória do primeiro jogador. Com um jogo perfeito , o primeiro jogador pode forçar uma vitória, na ou antes da 41ª jogada ( dobra ), começando na coluna do meio. O jogo é um empate teórico quando o primeiro jogador começa nas colunas adjacentes ao centro. Para as bordas do tabuleiro de jogo, colunas 1 e 2 à esquerda (ou colunas 7 e 6 à direita), a pontuação exata do valor do movimento para o início do primeiro jogador é a perda no 40º movimento e a perda no 42º movimento , respectivamente. Em outras palavras, começando com as quatro colunas externas, o primeiro jogador permite que o segundo jogador force uma vitória.

## Variações de regra

Existem muitas variações do Ligue 4 com diferentes tamanhos de tabuleiro de jogo, peças de jogo e / ou regras de jogo. Muitas variações são populares na teoria dos jogos e na pesquisa de inteligência artificial, em vez de nos tabuleiros físicos e na jogabilidade das pessoas.

O tamanho da placa do Ligue 4 mais comumente usado é 7 colunas × 6 linhas. As variações de tamanho incluem 5 × 4, 6 × 5, 8 × 7, 9 × 7, 10 × 7, 8 × 8, Infinite Connect-Four, e Cylinder-Infinite Connect-Four.

Várias versões do tabuleiro de jogo físico Connect Four da Hasbro facilitam a remoção de peças do jogo, uma de cada vez. Juntamente com a jogabilidade tradicional , esse recurso permite variações do jogo. Algumas versões anteriores de jogos também incluíam discos especialmente marcados e extensores de colunas de papelão, para variações adicionais ao jogo.

### **PopOut**

O *PopOut* começa da mesma maneira que na jogabilidade tradicional, com um tabuleiro vazio e os jogadores alternando turnos, colocando seus próprios discos coloridos no tabuleiro. Durante cada turno, um jogador pode adicionar outro disco a partir do topo ou, se houver algum disco de sua própria cor na linha inferior, remover (ou "retirar") um disco de sua própria cor a partir do fundo. Retirar um disco da parte inferior coloca todos os discos acima dele em um espaço, alterando o relacionamento com o restante do quadro e alterando as possibilidades de conexão. O primeiro jogador a conectar quatro de seus discos na horizontal, na vertical ou na diagonal vence o jogo.

### **Pop 10**

Antes do início do jogo, o *Pop 10* é configurado de maneira diferente do jogo tradicional. Revezando-se, cada jogador coloca um de seus próprios discos coloridos nos slots, preenchendo apenas a linha inferior, passando para a próxima linha até que ela seja preenchida, e assim sucessivamente até que todas as linhas tenham sido preenchidas.

A jogabilidade funciona por jogadores que se revezam removendo um disco de sua própria cor através da parte inferior do tabuleiro. Se o disco que foi removido fazia parte de uma conexão de quatro discos no momento da remoção, o aparelho o deixa de lado e imediatamente faz outra jogada. Se não fazia parte de um Ligue 4, deve ser colocado de volta no tabuleiro através de um slot na parte superior em qualquer espaço aberto em uma coluna alternativa (sempre que possível) e o turno termina, alternando para o outro jogador. O primeiro jogador a reservar dez discos da sua cor vence o jogo.

### **5 em linha**

A variação de 5 em linha para o Connect Four é um jogo jogado em uma grade de 6 de altura e 9 de largura. A Hasbro adiciona duas colunas adicionais do tabuleiro, já preenchidas com peças de jogadores em um padrão alternado, aos lados esquerdo e direito do tabuleiro padrão de 6 por 7. O jogo é semelhante ao Connect Four original, exceto que os jogadores agora precisam obter cinco peças seguidas para ganhar. Observe que este ainda é um jogo de 42 camadas, pois as duas novas colunas adicionadas ao jogo representam doze peças de jogo já jogadas, antes do início de um jogo.

### **Power Up**

Nesta variação do Ligue 4, os jogadores iniciam um jogo com uma ou mais peças de jogo "Power Checkers" especialmente marcadas, que cada jogador pode escolher jogar uma vez por jogo. Ao jogar uma peça marcada com um ícone de bigorna, por exemplo, o jogador pode retirar imediatamente todas as peças abaixo dela, deixando a peça de bigorna na linha inferior do tabuleiro de jogo. Outras peças de jogo marcadas incluem uma com um ícone de parede, permitindo que um jogador jogue um segundo turno consecutivo sem vencer com uma peça não marcada; um ícone "× 2", permitindo um segundo turno sem restrições com uma peça não marcada; e um ícone de bomba, permitindo que um jogador saia imediatamente da peça de um oponente.
